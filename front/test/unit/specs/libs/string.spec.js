import { expect } from 'chai'

import { hasContent } from 'src/libs/strings'

describe('hasContent should', () => {
  it('be false when parameter is null', () => {
    expect(hasContent(null)).to.be.false
  })
  it('be false when empty string', () => {
    expect(hasContent('')).to.be.false
  })
  it('be false when only spaces', () => {
    expect(hasContent('   ')).to.be.false
  })
  it('be true when string has content', () => {
    expect(hasContent(' a  ')).to.be.true
  })
})
