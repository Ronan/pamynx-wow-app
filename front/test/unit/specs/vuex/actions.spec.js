import { loadLabels } from 'src/vuex/actions'
import { mock } from 'src/libs/mock/fetch.mock'
import { check } from './actions.checker'

describe('actions', () => {
  it('loadLabels should dispatch a SET_LABELS mutation', (done) => {
    check(loadLabels, [], {}, [
      { name: 'SET_LABELS', payload: [mock.data.labels] }
    ], done)
  })
})
