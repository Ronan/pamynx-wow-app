export const hasContent = (str) => {
  let res = false
  if (str && str.trim().length > 0) {
    res = true
  }
  return res
}
