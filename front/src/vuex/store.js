import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  labels: {
    login: {},
    about: {}
  },
  logged: false
}

export const mutations = {
  SET_LABELS (state, labels) {
    state.labels = labels
  },
  SET_LOGIN (state, status) {
    state.logged = status
  }
}

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state,
  mutations
})
