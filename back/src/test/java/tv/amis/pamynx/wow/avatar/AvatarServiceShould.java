package tv.amis.pamynx.wow.avatar;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Duration;

import org.apache.http.client.ClientProtocolException;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import tv.amis.pamynx.wow.utils.ImageService;

public class AvatarServiceShould {

	private AvatarService service;
	
	@Before
	public void before() {
		service = new AvatarService(new ImageService(), "./target", Duration.ofHours(3l), Duration.ofHours(1l), Duration.ofSeconds(1l));
	}
	
	@Test
	public void retreive_image_for_character() throws ClientProtocolException, URISyntaxException, IOException {
		BufferedImage img = service.getAvatar("eu", "Sargeras", "Pamynx");
		Assert.assertThat(img, Matchers.notNullValue());
	}

	@Test
	public void retreive_the_guild_roster() throws ClientProtocolException, URISyntaxException, IOException {
		BufferedImage img = service.getRoster();
		Assert.assertThat(img, Matchers.notNullValue());
	}
}
