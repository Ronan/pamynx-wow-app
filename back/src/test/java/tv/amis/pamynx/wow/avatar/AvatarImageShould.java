package tv.amis.pamynx.wow.avatar;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.junit.Test;

import tv.amis.pamynx.wow.utils.ImageService;

public class AvatarImageShould {

	private ImageService service = new ImageService();
	
	@Test
	public void retreive_image_for_character() throws AvatarException, IOException {
		AvatarImage img = new AvatarImage(new AvatarKey("eu", "Sargeras", "Pamynx"));
		BufferedImage data = img.getImg(true);
		assertThat(service.imgToBytes(data).length, greaterThan(0));
	}
}
