package tv.amis.pamynx.wow.battlenet;

import java.util.Locale;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import tv.amis.pamynx.wow.App;
import tv.amis.pamynx.wow.battlenet.beans.WowCharacter;
import tv.amis.pamynx.wow.battlenet.beans.WowGuild;
import tv.amis.pamynx.wow.battlenet.beans.WowGuildMember;

public class BattleNetServiceShould {

	private ConfigurableApplicationContext app;
	private BattleNetService service;

	@Before
	public void before() {
		app = new SpringApplicationBuilder()
	        .sources(App.class)
	        .run();
		service = new BattleNetService("8vkxyhwqkb787e47utga6r5djuw2unqt", Locale.ENGLISH);
	}
	
	@After
	public void after() {
		app.close();
	}

	@Test
	public void retreive_character_data() throws BattleNetException {
		WowCharacter pamynx = service.getCharacter("eu", "Sargeras", "Pamynx");

		Assert.assertThat(pamynx.getName(), Matchers.is("Pamynx"));
		Assert.assertThat(pamynx.getAverageItemLevelEquipped(), Matchers.greaterThan(0l));
		Assert.assertThat(pamynx.getAchievementPoints(), Matchers.greaterThan(0l));
	}

	@Test
	public void retreive_guild_data() throws BattleNetException {
		WowGuild sapins = service.getGuild("eu", "Sargeras", "Les Sapins de la Horde");

		Assert.assertThat(sapins.getMembers().size(), Matchers.greaterThan(0));
	}
	
	@Test
	public void list_rerolls() throws BattleNetException {
		WowGuild sapins = service.getGuild("eu", "Sargeras", "Les Sapins de la Horde");
		
		for (WowGuildMember member : sapins.getMembers().stream().limit(10).collect(Collectors.toSet())) {
			try {
				WowCharacter character = service.getCharacter("eu", member.getRealm(), member.getName());
				System.out.println("Retreived member "+member.getName()+"-"+member.getRealm());
				Assert.assertEquals(member.getName(), character.getName());
			} catch (BattleNetException bne) {
				System.out.println("Cannot retreive member "+member.getName()+"-"+member.getRealm());
			}
		}
	}
}
