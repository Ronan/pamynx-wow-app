package tv.amis.pamynx.wow.battlenet;

import org.apache.log4j.Logger;

public class App {

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());

    public static void main(String[] args) {
        log.trace("trace");
        log.debug("debug");
        log.info("info");
        log.warn("warning");
        log.error("error");
    }

}
