package tv.amis.pamynx.wow.battlenet;

public class BattleNetException extends Exception {

	public BattleNetException(Exception e) {
		super(e);
	}
}
