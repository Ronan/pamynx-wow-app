package tv.amis.pamynx.wow.avatar;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.time.Duration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leroy.ronan.cache.Cache;
import com.leroy.ronan.cache.memory.InMemoryCacheBuilder;
import com.leroy.ronan.cache.persisted.FilePersistedCacheBuilder;

import tv.amis.pamynx.wow.AppConfiguration;
import tv.amis.pamynx.wow.utils.ImageService;

@Service
public class AvatarService {

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());
	
	private Cache<AvatarKey, BufferedImage> memory;

	private ImageService img;
	private String rootDir;

	@Autowired
	public AvatarService(ImageService img, AppConfiguration config) {
		this(img, config.getAvatarRootDir(), config.getAvatarFileDuration(), config.getAvatarMemoryDuration(), config.getAvatarTimeoutDuration());
	}
	
	public AvatarService(ImageService img, String rootDir, Duration fileDuration, Duration memoryDuration, Duration timeout) {
    	this.img = img;
    	this.rootDir = rootDir;
    	
		Cache<AvatarKey, BufferedImage> fileCache = new FilePersistedCacheBuilder<AvatarKey, BufferedImage>()
				.storingKey(this::keyToFile)
				.convertingData(img::imgToBytes)
				.recoveringData(img::imgFromBytes)
				.from(this::makeAvatar)
				.expiringAfter(fileDuration)
				.build();

		memory = new InMemoryCacheBuilder<AvatarKey, BufferedImage>()
				.from(fileCache::get)
				.expiringAfter(memoryDuration)
				.timingOutAfter(timeout)
				.build();
	}

	private File keyToFile(AvatarKey key) {
		File f = Paths.get(rootDir+"/notfound.png").toFile();
		try {
			f = Paths.get(
						rootDir+"/"
						+URLEncoder.encode(key.getZone(), "UTF-8")+"/"
						+URLEncoder.encode(key.getRealm(), "UTF-8")+"/"
						+URLEncoder.encode(key.getCharacter(), "UTF-8")
						+".png"
					)
					.toFile();
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		}
		f.getParentFile().mkdirs();
		return f;
	}
	
	private BufferedImage makeAvatar(AvatarKey key) {
		BufferedImage res = null;
		try {
			AvatarImage avatar = new AvatarImage(key);
			res = avatar.getImg(true);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return res;
	}
	
	public BufferedImage getAvatar(String zone, String realm, String character) {
		return memory.get(new AvatarKey(zone, realm, character));
	}
	
	public BufferedImage getPamynxAndCo() {
		return img.combineLeftToRight(
				getAvatar("eu", "Sargeras", "Pamynx"),
				getAvatar("eu", "Sargeras", "Gromalynx"),
				getAvatar("eu", "Sargeras", "Pamyniste"),
				getAvatar("eu", "Sargeras", "Aphykith"),
				getAvatar("eu", "Sargeras", "Pamynette"),
				getAvatar("eu", "Sargeras", "Pamonynx"),
				getAvatar("eu", "Sargeras", "Pamladynx"),
				getAvatar("eu", "Sargeras", "Pamalynx"),
				getAvatar("eu", "Sargeras", "Oldpamy")
			);
	}

	public BufferedImage getRoster() {

		BufferedImage aenariel    = getAvatar("eu", "Sargeras", "Aenariel");
		BufferedImage blacktaurus = getAvatar("eu", "Garona",   "Blacktaurus");
		BufferedImage berenos     = getAvatar("eu", "Sargeras", "Berenos");
		BufferedImage damijaw     = getAvatar("eu", "Sargeras", "Damijaw");
		BufferedImage djar        = getAvatar("eu", "Ner'zhul", "Djar");
		BufferedImage drynn       = getAvatar("eu", "Ner'zhul", "Drynn");
		BufferedImage flavione    = getAvatar("eu", "Sargeras", "Flavione");
		BufferedImage hatok       = getAvatar("eu", "Sargeras", "Hatok");
		BufferedImage hiei        = getAvatar("eu", "Sargeras", "Hïei");
		BufferedImage jul         = getAvatar("eu", "Sargeras", "Jül");
		BufferedImage kurina      = getAvatar("eu", "Sargeras", "Kurina");
		BufferedImage meuharf     = getAvatar("eu", "Ner'zhul", "Meuharf");
		BufferedImage mockingbird = getAvatar("eu", "Sargeras", "Møckingbird");
		BufferedImage narawiel    = getAvatar("eu", "Sargeras", "Narawiel");
		BufferedImage pamynx      = getAvatar("eu", "Sargeras", "Pamynx");
		BufferedImage popaheal    = getAvatar("eu", "Sargeras", "Pøpaheal");
		BufferedImage sharkilol   = getAvatar("eu", "Ner'zhul", "Sharkilol");
		BufferedImage snomeadh    = getAvatar("eu", "Sargeras", "Snomeadh");
		BufferedImage trolede     = getAvatar("eu", "Garona",   "Trolede");
		BufferedImage yuki        = getAvatar("eu", "Sargeras", "Yùkï");

		return img.combineTopToBottom(
				img.combineLeftToRight(aenariel, blacktaurus, berenos, damijaw, djar),
				img.combineLeftToRight(drynn, flavione, hatok, hiei, jul),
				img.combineLeftToRight(kurina, meuharf, mockingbird, narawiel, pamynx),
				img.combineLeftToRight(popaheal, sharkilol, snomeadh, trolede, yuki)
			);
	}

}
