package tv.amis.pamynx.wow.utils;

import java.io.Closeable;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class SimpleHttpClient implements Closeable{

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());

    private CloseableHttpClient httpclient;
    
    public SimpleHttpClient() {
	    BasicCookieStore cookieStore = new BasicCookieStore();
	    HttpClientBuilder builder = HttpClients.custom().setDefaultCookieStore(cookieStore);
	    this.httpclient = builder.build();
    }
    
	@Override
	public void close() throws IOException {
		httpclient.close();
	}
    
    public String get(String uri) throws IOException, ClientProtocolException {
        String res = null;

        log.debug("http get : "+uri);

        HttpGet get = new HttpGet(uri);
        try (CloseableHttpResponse response = httpclient.execute(get)) {
            HttpEntity entity = response.getEntity();
            res = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
        }
        return res;
    }

    public String post(String uri, NameValuePair...params) throws URISyntaxException, ClientProtocolException, IOException{
        String res = null;

        log.debug("http post : "+uri);

        HttpUriRequest post = RequestBuilder.post()
                .setUri(uri)
                .setEntity(new UrlEncodedFormEntity(Arrays.asList(params), Charset.forName("UTF-8")))
                .build();

        try (CloseableHttpResponse response = httpclient.execute(post)) {
            HttpEntity entity = response.getEntity();
            res = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
        }
        return res;
    }
}
