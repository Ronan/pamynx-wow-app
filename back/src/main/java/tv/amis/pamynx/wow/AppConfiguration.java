package tv.amis.pamynx.wow;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfiguration {
	
	@Value("${tv.amis.pamynx.wow.avatar.rootdir}")
	private String avatarRootDir;
	
	@Value("${tv.amis.pamynx.wow.avatar.duration.file.value}")
	private int avatarDurationFileValue;
	@Value("${tv.amis.pamynx.wow.avatar.duration.file.unit}")
	private String avatarDurationFileUnit;
	
	@Value("${tv.amis.pamynx.wow.avatar.duration.memory.value}")
	private int avatarDurationMemoryValue;
	@Value("${tv.amis.pamynx.wow.avatar.duration.memory.unit}")
	private String avatarDurationMemoryUnit;

	@Value("${tv.amis.pamynx.wow.avatar.duration.timeout.value}")
	private int avatarDurationTimeoutValue;
	@Value("${tv.amis.pamynx.wow.avatar.duration.timeout.unit}")
	private String avatarDurationTimeoutUnit;

	public String getAvatarRootDir() {
		return avatarRootDir;
	}
	
	public Duration getAvatarFileDuration() {
		return Duration.of(avatarDurationFileValue, ChronoUnit.valueOf(avatarDurationFileUnit));
	}
	public Duration getAvatarMemoryDuration() {
		return Duration.of(avatarDurationMemoryValue, ChronoUnit.valueOf(avatarDurationMemoryUnit));
	}

	public Duration getAvatarTimeoutDuration() {
		return Duration.of(avatarDurationTimeoutValue, ChronoUnit.valueOf(avatarDurationTimeoutUnit));
	}
}
