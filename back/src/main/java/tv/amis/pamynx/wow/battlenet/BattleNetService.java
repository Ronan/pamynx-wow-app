package tv.amis.pamynx.wow.battlenet;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Locale;

import org.apache.log4j.Logger;

import tv.amis.pamynx.wow.battlenet.beans.WowCharacter;
import tv.amis.pamynx.wow.battlenet.beans.WowGuild;
import tv.amis.pamynx.wow.utils.SimpleHttpClient;

public class BattleNetService {

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());

	private String apikey;
	private String locale;
	
	public BattleNetService(String apikey, Locale locale) {
		super();
		this.apikey = apikey;
		this.locale = locale.toString();
	}

	public WowCharacter getCharacter(String region, String realm, String name) throws BattleNetException {
		WowCharacter res = null;
        try (SimpleHttpClient httpclient = new SimpleHttpClient()){
			String json = httpclient.get("https://"+region+".api.battle.net/wow/character/"+realm+"/"+URLEncoder.encode(name, "UTF-8").replace("+", "%20")+"?fields=items&locale="+locale+"&apikey="+apikey);
			res = new WowCharacter(json);
        } catch (IOException e) {
        	log.error(e.getMessage(), e);
        	throw new BattleNetException(e);
		}
        return res;
	}

	public WowGuild getGuild(String region, String realm, String name) throws BattleNetException {
		WowGuild res = null;
        try (SimpleHttpClient httpclient = new SimpleHttpClient()){
        	String url = "https://"+region+".api.battle.net/wow/guild/"+realm+"/"+URLEncoder.encode(name, "UTF-8").replace("+", "%20")+"?fields=members&locale="+locale+"&apikey="+apikey;
			String json = httpclient.get(url);
			res = new WowGuild(json);
        } catch (IOException e) {
        	log.error(e.getMessage(), e);
        	throw new BattleNetException(e);
		}
        return res;
	}
}
