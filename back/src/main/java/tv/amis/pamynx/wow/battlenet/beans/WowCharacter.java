package tv.amis.pamynx.wow.battlenet.beans;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import tv.amis.pamynx.wow.battlenet.BattleNetException;

public class WowCharacter extends WowJson {

	public static final int MAXLEVEL = 110;
	
    private Long lastModified;
    private String name;
    private String realm;
    private String battlegroup;
    
    private Long level;
    
    private Long achievementPoints;
    
    private Long averageItemLevel;
    private Long averageItemLevelEquipped;
    
    public WowCharacter(String json) throws BattleNetException {
        super(json);
        
        JsonObject obj;
        try (JsonReader reader = Json.createReader(new StringReader(json))) {
            obj = reader.readObject();
        } 
        if (obj.get("status") == null || "ok".equals(obj.getString("status"))) {
	        lastModified = obj.getJsonNumber("lastModified").longValueExact();
	        name = obj.getString("name");
	        realm = obj.getString("realm");
	        battlegroup = obj.getString("battlegroup");
	        achievementPoints = obj.getJsonNumber("achievementPoints").longValueExact();
	        level = obj.getJsonNumber("level").longValueExact();
	        
	        JsonObject items = obj.getJsonObject("items");
	        if (items != null){
	        	averageItemLevel = items.getJsonNumber("averageItemLevel").longValueExact();
	        	averageItemLevelEquipped = items.getJsonNumber("averageItemLevelEquipped").longValueExact();
	        }
        } else {
        	throw new BattleNetException(new IllegalArgumentException(json));
        }
    }

    public Long getLastModified() {
        return lastModified;
    }

    public String getName() {
        return name;
    }

    public String getRealm() {
        return realm;
    }

    public String getBattlegroup() {
        return battlegroup;
    }

    public Long getLevel() {
		return level;
	}

	public Long getAverageItemLevel() {
        return averageItemLevel;
    }

    public Long getAverageItemLevelEquipped() {
        return averageItemLevelEquipped;
    }

    public Long getAchievementPoints() {
        return achievementPoints;
    }

}
