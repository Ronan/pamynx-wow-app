package tv.amis.pamynx.wow.battlenet.beans;

import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;

public class WowGuild extends WowJson{

    private Map<Long, Set<WowGuildMember>> members;
    
    public WowGuild(String json) {
        super(json);

        this.members = new HashMap<>();
        JsonObject obj = Json.createReader(new StringReader(json)).readObject();
        JsonArray jsonMembers = obj.getJsonArray("members");
        for (int i = 0; i < jsonMembers.size(); i++) {
        	JsonObject cur = jsonMembers.getJsonObject(i);

            Long rank = cur.getJsonNumber("rank").longValue();
            
            JsonObject character = cur.getJsonObject("character");
            String realm = character.getString("realm");
            String name = character.getString("name");
            
            WowGuildMember curMember = new WowGuildMember(rank, realm, name);
            Set<WowGuildMember> rankList = this.members.get(rank);
            if (rankList == null){
                rankList = new HashSet<>();
                this.members.put(rank,  rankList);
            }
            rankList.add(curMember);
        }
    }

    public Set<WowGuildMember> getMembers() {
        return this.members.values()
                    .stream()
                    .flatMap(s -> s.stream())
                    .collect(Collectors.toSet());
    }

}
