package tv.amis.pamynx.wow.battlenet.beans;

public class WowJson {

	private String json;

	public WowJson(String json) {
		super();
		this.json = json;
	}

	public String getJson() {
		return json;
	}
}
