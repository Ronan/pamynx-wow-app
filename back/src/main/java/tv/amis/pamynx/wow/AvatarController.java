package tv.amis.pamynx.wow;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tv.amis.pamynx.wow.avatar.AvatarService;

@Controller
@RequestMapping(value="/avatar/{zone}/{realm}/{character}.png")
public class AvatarController {

	private AvatarService service;

    @Autowired
    public AvatarController(AvatarService service) {
        this.service = service;
    }

	@ResponseBody
	@RequestMapping(method=RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
	public byte[] avatar(@PathVariable String zone, @PathVariable String realm, @PathVariable String character) throws IOException, URISyntaxException {
        byte[] imageInByte;
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			BufferedImage data = service.getAvatar(zone, realm, character);
			ImageIO.write(data, "png", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
		}
		return imageInByte;
	}
}
