package tv.amis.pamynx.wow.avatar;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import tv.amis.pamynx.wow.utils.SimpleHttpClient;

public class AvatarImage {

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());

    private String zone;
    private String realm;
    private String character;

    public AvatarImage(AvatarKey key){
        this.zone = key.getZone();
        this.realm = key.getRealm();
        this.character = key.getCharacter();
    }

    public String getZone() {
        return zone;
    }

    public String getRealm() {
        return realm;
    }

    public String getCharacter() {
        return character;
    }
    
    public BufferedImage getImg(boolean useIlvl) throws AvatarException {
        log.debug("getImg("+zone+","+realm+","+character+")");

        BufferedImage res = null;

        try (SimpleHttpClient httpclient = new SimpleHttpClient()){
        	httpclient.get("http://www.best-signatures.com/api/?region="+zone+"&realm="+realm+"&char="+character+"&type=Sign9&preview=1&c1=class");

        	httpclient.get("http://www.best-signatures.com/wow/");

        	httpclient.post( 
                    "http://www.best-signatures.com/ajax/generator/load/", 
                    new BasicNameValuePair("region", zone),
                    new BasicNameValuePair("server", realm),
                    new BasicNameValuePair("char", character), // Multi : StringUtils.join(characters, ","))
                    new BasicNameValuePair("lang", "en_GB")
                    );
        	httpclient.post( 
                    "http://www.best-signatures.com/ajax/generator/settype/", 
                    new BasicNameValuePair("signType", "Sign22")
                    );
        	httpclient.post( 
                    "http://www.best-signatures.com/ajax/generator/setstyle/", 
                    new BasicNameValuePair("style[avatar][type]", "portrait"),
                    new BasicNameValuePair("style[color][1]", "#FFFFFF"),
                    new BasicNameValuePair("style[effect]", "noeffect"),
                    new BasicNameValuePair("style[other][name]", "1"),
                    new BasicNameValuePair("style[other][classcolor]", "1"),
                    new BasicNameValuePair("style[other][race]", "0"),
                    new BasicNameValuePair("style[other][ilvl]", useIlvl?"1":"0")
                    );
            String json = httpclient.post( 
                    "http://www.best-signatures.com/ajax/generator/save/", 
                    new BasicNameValuePair("save", "1")
                    );
            
            JsonObject obj;
            try (JsonReader reader = Json.createReader(new StringReader(json))) {
                obj = reader.readObject();
            }
            String link = obj.getString("link");
            log.debug("link:"+link);
            res = ImageIO.read(new URL(link));
        } catch (IOException | URISyntaxException e) {
        	log.error(e.getMessage(), e);
        	throw new AvatarException(e);
		}
        return res;
    }
}
