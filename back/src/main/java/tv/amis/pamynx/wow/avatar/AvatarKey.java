package tv.amis.pamynx.wow.avatar;

public class AvatarKey {

	private String zone;
	private String realm;
	private String character;

	public AvatarKey(String zone, String realm, String character) {
		super();
		this.zone = zone;
		this.realm = realm;
		this.character = character;
	}

	protected String getZone() {
		return zone;
	}

	protected String getRealm() {
		return realm;
	}

	protected String getCharacter() {
		return character;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((character == null) ? 0 : character.hashCode());
		result = prime * result + ((realm == null) ? 0 : realm.hashCode());
		result = prime * result + ((zone == null) ? 0 : zone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvatarKey other = (AvatarKey) obj;
		if (character == null) {
			if (other.character != null)
				return false;
		} else if (!character.equals(other.character))
			return false;
		if (realm == null) {
			if (other.realm != null)
				return false;
		} else if (!realm.equals(other.realm))
			return false;
		if (zone == null) {
			if (other.zone != null)
				return false;
		} else if (!zone.equals(other.zone))
			return false;
		return true;
	}
}
