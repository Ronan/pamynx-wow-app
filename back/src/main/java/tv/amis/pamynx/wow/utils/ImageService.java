package tv.amis.pamynx.wow.utils;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ImageService {

    private static final Logger log = Logger.getLogger(new Object() { }.getClass().getEnclosingClass());

	public byte[] imgToBytes(BufferedImage img) {
		byte[] res = null;
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			ImageIO.write(img, "png", baos);
			baos.flush();
			res = baos.toByteArray();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return res;
	}
	
	public BufferedImage imgFromBytes(byte[] img) {
		BufferedImage res = null;
		try (InputStream in = new ByteArrayInputStream(img)){
			res = ImageIO.read(in);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return res;
	}
	
	public BufferedImage combineLeftToRight(BufferedImage...images) {
		return Arrays.stream(images)
			.collect(Collectors.reducing((img1, img2) -> reduceLeftToRight(img1, img2)))
			.orElse(null);
	}
	
	public BufferedImage combineTopToBottom(BufferedImage...images) {
		return Arrays.stream(images)
			.collect(Collectors.reducing((img1, img2) -> reduceTopToBottom(img1, img2)))
			.orElse(null);
	}
	
	private BufferedImage reduceLeftToRight(BufferedImage image1, BufferedImage image2) {
		BufferedImage res;
		if (image1 == null){
			res = image2;
		} else if (image2 == null){
			res = image1;
		} else {
			int width = image1.getWidth() + image2.getWidth();
			int height = Math.max(image1.getHeight(), image2.getHeight());
		    res = new BufferedImage(width, height, image1.getType());

		    Graphics g = res.getGraphics();
		    g.drawImage(image1, 0, 0, null);
		    g.drawImage(image2, image1.getWidth(), 0, null);
		}
        return res;
	}
	
	private BufferedImage reduceTopToBottom(BufferedImage image1, BufferedImage image2) {
		BufferedImage res;
		if (image1 == null){
			res = image2;
		} else if (image2 == null){
			res = image1;
		} else {
			int width = Math.max(image1.getWidth(), image2.getWidth());
			int height = image1.getHeight() + image2.getHeight();
		    res = new BufferedImage(width, height, image1.getType());

		    Graphics g = res.getGraphics();
		    g.drawImage(image1, 0, 0, null);
		    g.drawImage(image2, 0, image1.getHeight(), null);
		}
        return res;
	}

}
