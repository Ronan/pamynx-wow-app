package tv.amis.pamynx.wow.avatar;

public class AvatarException extends Exception {

	public AvatarException(Exception e) {
		super(e);
	}
}
