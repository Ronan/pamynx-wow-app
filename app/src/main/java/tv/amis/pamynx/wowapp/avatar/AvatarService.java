package tv.amis.pamynx.wowapp.avatar;

import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.json.Json;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

@Service
public class AvatarService {

    private static final Logger log = LoggerFactory.getLogger(new Object() { }.getClass().getEnclosingClass());

    @Autowired
    public AvatarService() {
    }

    @Cacheable("avatars")
    public BufferedImage getAvatar(String zone, String realm, String character) throws IOException {
        log.debug("getImg("+zone+","+realm+","+character+")");

        try (var httpclient = new SimpleHttpClient()){
            httpclient.get("http://www.best-signatures.com/api/?region="+zone+"&realm="+realm+"&char="+character+"&type=Sign9&preview=1&c1=class");

            httpclient.get("http://www.best-signatures.com/wow/");

            httpclient.post(
                "http://www.best-signatures.com/ajax/generator/load/",
                new BasicNameValuePair("region", zone),
                new BasicNameValuePair("server", realm),
                new BasicNameValuePair("char", character), // Multi : StringUtils.join(characters, ","))
                new BasicNameValuePair("lang", "en_GB")
            );
            httpclient.post(
                "http://www.best-signatures.com/ajax/generator/settype/",
                new BasicNameValuePair("signType", "Sign22")
            );
            httpclient.post(
                "http://www.best-signatures.com/ajax/generator/setstyle/",
                new BasicNameValuePair("style[avatar][type]", "portrait"),
                new BasicNameValuePair("style[color][1]", "#FFFFFF"),
                new BasicNameValuePair("style[effect]", "noeffect"),
                new BasicNameValuePair("style[other][name]", "1"),
                new BasicNameValuePair("style[other][classcolor]", "1"),
                new BasicNameValuePair("style[other][race]", "0"),
                new BasicNameValuePair("style[other][ilvl]", "1") // "0" for lvl
            );
            String json = httpclient.post(
                "http://www.best-signatures.com/ajax/generator/save/",
                new BasicNameValuePair("save", "1")
            );

            try (var reader = Json.createReader(new StringReader(json))) {
                var obj = reader.readObject();
                var link = obj.getString("link");
                log.debug("link:"+link);
                return ImageIO.read(new URL(link));
            }
        }
    }
}
