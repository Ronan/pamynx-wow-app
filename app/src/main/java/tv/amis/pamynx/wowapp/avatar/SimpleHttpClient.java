package tv.amis.pamynx.wowapp.avatar;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

public class SimpleHttpClient implements Closeable{

    private static final Logger log = LoggerFactory.getLogger(new Object() { }.getClass().getEnclosingClass());

    private CloseableHttpClient httpclient;
    
    public SimpleHttpClient() {
	    BasicCookieStore cookieStore = new BasicCookieStore();
	    HttpClientBuilder builder = HttpClients.custom().setDefaultCookieStore(cookieStore);
	    this.httpclient = builder.build();
    }
    
	@Override
	public void close() throws IOException {
		httpclient.close();
	}
    
    public String get(String uri) throws IOException {
        log.debug("http get : "+uri);
        HttpGet get = new HttpGet(uri);
        return execute(httpclient.execute(get));
    }

    public String post(String uri, NameValuePair...params) throws IOException{
        log.debug("http post : "+uri);
        HttpUriRequest post = RequestBuilder.post()
                .setUri(uri)
                .setEntity(new UrlEncodedFormEntity(Arrays.asList(params), Charset.forName("UTF-8")))
                .build();
        return execute(httpclient.execute(post));
    }

    private String execute(CloseableHttpResponse execute) throws IOException {
        try (CloseableHttpResponse response = execute) {
            HttpEntity entity = response.getEntity();
            String res = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
            return res;
        }
    }
}
