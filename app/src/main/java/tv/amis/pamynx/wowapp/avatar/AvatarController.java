package tv.amis.pamynx.wowapp.avatar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;

@Controller
public class AvatarController {

	private AvatarService service;

    @Autowired
    public AvatarController(AvatarService service) {
        this.service = service;
    }

	@ResponseBody
	@RequestMapping(
			method=RequestMethod.GET,
			produces = MediaType.IMAGE_PNG_VALUE,
			value="/avatar/{zone}/{realm}/{character}.png"
	)
	public byte[] avatar(@PathVariable String zone, @PathVariable String realm, @PathVariable String character)
        throws IOException {
    	try (var baos = new ByteArrayOutputStream()) {
			var data = service.getAvatar(zone, realm, character);
			ImageIO.write(data, "png", baos);
			baos.flush();
			return baos.toByteArray();
		}
	}
}
