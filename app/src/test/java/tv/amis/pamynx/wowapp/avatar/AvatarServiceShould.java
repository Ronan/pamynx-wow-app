package tv.amis.pamynx.wowapp.avatar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.image.BufferedImage;
import java.io.IOException;

class AvatarServiceShould {

    private AvatarService service;

    @BeforeEach
    void setUp() {
        service = new AvatarService();
    }

    @Test
    void retreive_image_for_character() throws IOException {
        BufferedImage img = service.getAvatar("eu", "Sargeras", "Pamynx");

        Assertions.assertNotNull(img);
    }

}
