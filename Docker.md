mvn docker:build
=> Construit l'image, la met dans docker.

docker images
=> Liste les images disponibles.

docker run -p 8090:8082 -t springio/wow-app-back

docker run -p 8082:8080 -v /home/pamynx/docker/data:/data -v /home/pamynx/docker/log:/log -t springio/wow-app-back
=> Lancer l'image springio/wow-app-back
=> Le port du container 8080 arrive sur le 8082 du host
=> Le volume /data arrive sur /home/pamynx/docker/data du host

docker ps
=> Liste les containers qui tournent.

docker ps -a
=> Liste tous les containers

docker rmi 9d64cf7d4862
=> Supprimer l'image  9d64cf7d4862

docker rm ee090031a432
=> Supprimer le containers ee090031a432

docker stop af2b76eb9945
=> Arrete l'image af2b76eb9945

docker save --output gs-spring-boot-docker.tar 0bec907c9ee2
=> Sauve l'image 0bec907c9ee2 dans gs-spring-boot-docker.tar.

docker exec -it bdbd80d0cd0e /bin/sh
=> Connexion shell sur le container
